# Variables that I will move to Params~
USER_NAME=itomaldonado
REPO_NAME=ovs1
APP_PORT=5000
# Docker-specific config
DOCKER_IMAGE_NAME="itomaldonado/ovs"
# Pull New Docker Version ...
echo "Pulling new version of image: $DOCKER_IMAGE_NAME"
sudo docker pull $DOCKER_IMAGE_NAME
# Get the currently running app instance.
APP_INSTANCE=`sudo docker ps | grep -wo " $REPO_NAME" | cut -d" " -f 2`
# If one instance found, bounce it.
if [ ! -z "$APP_INSTANCE" -a "$APP_INSTANCE" != "" -a "$APP_INSTANCE" != " " ]; then
    echo "Found instance with name: $APP_INSTANCE"

    # Stopping and starting our app.
    echo "Stopping and Removing instance with name: $APP_INSTANCE"
    sudo docker stop $REPO_NAME; sudo docker rm $REPO_NAME;

    echo "Starting new instance of image: $DOCKER_IMAGE_NAME with name: $APP_INSTANCE"
    sudo docker run -d --name $REPO_NAME -p $APP_PORT:80 $DOCKER_IMAGE_NAME
else
    echo "Instance with name: $REPO_NAME not found. Creating a new one."

    echo "Starting new instance of image: $DOCKER_IMAGE_NAME with name: $REPO_NAME"
    sudo docker run -d --name $REPO_NAME -p $APP_PORT:80 $DOCKER_IMAGE_NAME
fi


